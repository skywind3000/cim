#include <stdint.h>
#include <vector>

#include "internal.h"
#include "modules/include/flif.h"


int cim_flif_info(const void *data, int *w, int *h, int *bpp)
{
	int size = 1024 * 1024 * 10;
	FLIF_INFO *info = flif_read_info_from_memory(data, size);
	if (info == NULL) return -1;
	if (flif_info_num_images(info) == 0) {
		flif_destroy_info(info);
		return -2;
	}

	w[0] = flif_info_get_width(info);
	h[0] = flif_info_get_height(info);

	int n = flif_info_get_nb_channels(info);
	bpp[0] = (n == 4)? 32 : 24;

	flif_destroy_info(info);

	return 0;
}

int cim_flif_decode(const void *data, int nbytes, void *pixel, int stride)
{
	FLIF_DECODER *decoder = flif_create_decoder();	
	if (decoder == NULL) return -1;
	int hr = flif_decoder_decode_memory(decoder, data, nbytes);
	if (hr == 0) {
		flif_destroy_decoder(decoder);
		return -2;
	}
	FLIF_IMAGE *image = flif_decoder_get_image(decoder, 0);
	if (image == NULL) {
		flif_destroy_decoder(decoder);
		return -3;
	}
	int w = flif_image_get_width(image);
	int h = flif_image_get_height(image);
	int n = flif_image_get_nb_channels(image);
	n = (n == 4)? 4 : 3;
	std::vector<uint8_t> line;
	line.resize(w * 4 + 8);
	for (int j = 0; j < h; j++) {
		uint8_t *src = &line[0];
		uint8_t *dst = (uint8_t*)pixel;
		flif_image_read_row_RGBA8(image, j, src, line.size());
		if (n == 4) {
			for (int i = 0; i < w; i++, dst += 4, src += 4) {
				*((uint32_t*)dst) = *((uint32_t*)src);
			}
		}	else {
			for (int i = 0; i < w; i++, dst += 3, src += 4) {
				dst[0] = src[0];
				dst[1] = src[1];
				dst[2] = src[2];
			}
		}
		pixel = (char*)pixel + stride;
	}
	flif_destroy_decoder(decoder);
	return 0;
}


