#include "internal.h"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#endif

#include "modules/include/stb_image.h"



//---------------------------------------------------------------------
// get info from: png/bmp/jpg
//---------------------------------------------------------------------
int cim_stb_info(const void *data, int *w, int *h, int *bpp)
{
	int size = 10 * 1024 * 1024;
	int hr, n;
	hr = stbi_info_from_memory((const unsigned char*)data, size, w, h, &n);
	*bpp = (n == 4)? 32 : 24;
	if (hr == 0) return -1;
	return 0;
}


//---------------------------------------------------------------------
// decode png/bmp/jpg
//---------------------------------------------------------------------
int cim_stb_decode(const void *data, int nbytes, void *pixel, int stride)
{
	const unsigned char *source = (const unsigned char*)data;
	unsigned char *dest = (unsigned char*)pixel;
	unsigned char *bitmap = NULL;
	int w, h, n, hr;
	int size = nbytes;
	hr = stbi_info_from_memory(source, size, &w, &h, &n);
	if (hr == 0) return -1;
	n = (n == 4)? 4 : 3;
	bitmap = stbi_load_from_memory(source, size, &w, &h, &n, n);
	if (bitmap == NULL) return -2;
	for (int j = 0; j < h; j++) {
		memcpy(dest, bitmap, n * w);
		dest += stride;
		bitmap += n * w;
	}	
	stbi_image_free(bitmap);
	return 0;
}



//---------------------------------------------------------------------
// load content to string
//---------------------------------------------------------------------
bool cim_load_content(const char *filename, std::string &content)
{
	size_t size;
	FILE *fp = fopen(filename, "rb");
	if (fp == NULL) return false;
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	content.resize(size);
	if (size > 0) {
		char *ptr = &content[0];
		while (size > 0) {
			size_t hr = fread(ptr, 1, size, fp);
			if (hr == 0) {
				fclose(fp);
				return false;
			}
			size -= hr;
			ptr += hr;
		}
	}
	fclose(fp);
	return true;
}



//---------------------------------------------------------------------
// detect file type
//---------------------------------------------------------------------
int cim_image_detect(const void *data)
{
	const uint8_t *ptr = (const uint8_t*)data;
	static uint8_t png_head[] = { 137, 80, 78, 71, 13, 10, 26, 10 };
	static uint8_t bpg_head[] = { 0x42, 0x50, 0x47 };
	static uint8_t flif_head[] = { 0x46, 0x4c, 0x49, 0x46 };
	static uint8_t bmp_head[] = { 0x42, 0x4d };
	static uint8_t jpeg_head[] = { 0xff, 0xd8, 0xff };
	if (memcmp(ptr, bmp_head, sizeof(bmp_head)) == 0)
		return PIC_BMP;
	if (memcmp(ptr, jpeg_head, sizeof(jpeg_head)) == 0)
		return PIC_JPEG;
	if (memcmp(ptr, bpg_head, sizeof(bpg_head)) == 0)
		return PIC_BPG;
	if (memcmp(ptr, flif_head, sizeof(flif_head)) == 0)
		return PIC_FLIF;
	if (memcmp(ptr, png_head, sizeof(png_head)) == 0)
		return PIC_PNG;
	return PIC_UNKNOW;
}



