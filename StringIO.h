//=====================================================================
// 
// stringio.h - StringIO model interface
//
// NOTE:
// for more information, please see the readme file
//
//=====================================================================

#ifndef _STRINGIO_H_
#define _STRINGIO_H_

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef _MSC_VER
#pragma warning(disable:4530)
#endif

#ifndef __cplusplus
#error This file can only be compiled in c++ mode !!
#endif

#include <vector>
#include <stdexcept>

#ifndef __IINT64_DEFINED
#define __IINT64_DEFINED
#if defined(_MSC_VER) || defined(__BORLANDC__)
typedef __int64 IINT64;
#else
typedef long long IINT64;
#endif
#endif

#ifndef __IUINT64_DEFINED
#define __IUINT64_DEFINED
#if defined(_MSC_VER) || defined(__BORLANDC__)
typedef unsigned __int64 IUINT64;
#else
typedef unsigned long long IUINT64;
#endif
#endif


//=====================================================================
// CStringIO
//=====================================================================
class CStringIO
{
public:
	inline CStringIO();                        // initialize
	inline CStringIO(const CStringIO &sio);    // initialize

	inline bool empty() const;			// check if size equal to 0
	inline size_t size() const;			// get size

	inline char *data();				// get data ptr
	inline const char *data() const;	// get const data ptr
	inline void clear();				// set size to 0
	inline void reset();				// set size & pos to 0
	inline void resize(size_t size);	// resize (mem will not be released)
	inline void reserve(size_t len = 0);	// reserve memory

	inline size_t position() const;		// get current position
	inline void seek(size_t pos);		// set position to given value
	inline bool eof() const;			// check if position >= size
	inline void truncate();				// truncate at current position

	// write some bytes to current position
	inline void write(const void *ptr, size_t length);

	// read from current position, returns how many bytes acturelly read
	inline size_t read(void *ptr, size_t length);

	// returns how many bytes can be read
	inline size_t canread();

	inline const char & operator [] (size_t pos) const;		// reference
	inline char & operator[] (size_t pos);		// non-const reference

	// write & reading 
	inline void write_u8(unsigned char value);
	inline void write_u16(unsigned short value);
	inline void write_u32(unsigned long value);
	inline void write_i8(char value);
	inline void write_i16(short value);
	inline void write_i32(long value);

	inline unsigned char read_u8();
	inline unsigned short read_u16();
	inline unsigned long read_u32();
	inline char read_i8();
	inline short read_i16();
	inline long read_i32();

	inline void write_u64(IUINT64 value);
	inline void write_i64(IINT64 value);
	inline IUINT64 read_u64();
	inline IINT64 read_i64();

	// streaming 
	inline CStringIO& operator << (unsigned char value);
	inline CStringIO& operator << (unsigned short value);
	inline CStringIO& operator << (unsigned long value);
	inline CStringIO& operator << (unsigned int value);
	inline CStringIO& operator << (char value);
	inline CStringIO& operator << (short value);
	inline CStringIO& operator << (long value);
	inline CStringIO& operator << (int value);
	inline CStringIO& operator >> (unsigned char &value);
	inline CStringIO& operator >> (unsigned short &value);
	inline CStringIO& operator >> (unsigned long &value);
	inline CStringIO& operator >> (unsigned int &value);
	inline CStringIO& operator >> (char &value);
	inline CStringIO& operator >> (short &value);
	inline CStringIO& operator >> (long &value);
	inline CStringIO& operator >> (int &value);

	CStringIO& operator=(const CStringIO &);

protected:
	inline char *encode_u64(char *ptr, IUINT64 v);
	inline const char *decode_u64(const char *ptr, IUINT64 *v);

protected:	
	bool _little_endian;				// using little endian (lsb)
	std::vector<char> _data;			// data buffer
	size_t _size;						// size
	size_t _position;					// current position
};

//---------------------------------------------------------------------
// initialize
//---------------------------------------------------------------------
inline CStringIO::CStringIO()
{
	_data.resize(4);
	_size = 0;
	_position = 0;
	_little_endian = true;
}

//---------------------------------------------------------------------
// initialize
//---------------------------------------------------------------------
inline CStringIO::CStringIO(const CStringIO &sio)
{
	_data.resize(4);
	_size = 0;
	_position = 0;
	_little_endian = true;
	if (sio.size() > 0) {
		this->write(sio.data(), sio.size());
	}
}


//---------------------------------------------------------------------
// copy assignment
//---------------------------------------------------------------------
CStringIO& CStringIO::operator=(const CStringIO &sio)
{
	resize(sio.size());
	reset();
	if (sio.size() > 0) {
		write(sio.data(), sio.size());
	}
	return *this;
}


//---------------------------------------------------------------------
// check if size equal to 0
//---------------------------------------------------------------------
inline bool CStringIO::empty() const
{ 
	return (_size == 0);
}

//---------------------------------------------------------------------
// get size
//---------------------------------------------------------------------
inline size_t CStringIO::size() const
{ 
	return _size; 
}

//---------------------------------------------------------------------
// get data ptr
//---------------------------------------------------------------------
inline char *CStringIO::data()
{
	return &_data[0]; 
}

//---------------------------------------------------------------------
// get data ptr
//---------------------------------------------------------------------
inline const char *CStringIO::data() const
{
	return &_data[0]; 
}

//---------------------------------------------------------------------
// set size to 0
//---------------------------------------------------------------------
inline void CStringIO::clear() 
{
	_size = 0;
}

//---------------------------------------------------------------------
// reserve memory
//---------------------------------------------------------------------
inline void CStringIO::reserve(size_t size)
{
	size_t round = (size < 4)? 4 : size;
	if (round < _size) round = _size;
	_data.reserve(round);
}

//---------------------------------------------------------------------
// set size & pos to 0
//---------------------------------------------------------------------
inline void CStringIO::reset()
{
	_size = 0;
	_position = 0;
}

//---------------------------------------------------------------------
// resize (mem will not be released)
//---------------------------------------------------------------------
inline void CStringIO::resize(size_t size) 
{ 
	if (size > _data.size()) {
		size_t round;
		for (round = 1; round < size; ) round <<= 1;
		if (round < 8) round = 8;
		_data.resize(round);
	}
	_size = size;
}

//---------------------------------------------------------------------
// get current position
//---------------------------------------------------------------------
inline size_t CStringIO::position() const
{
	return _position;
}

//---------------------------------------------------------------------
// set position to given value
//---------------------------------------------------------------------
inline void CStringIO::seek(size_t pos)
{
	_position = pos;
}

//---------------------------------------------------------------------
// truncate at current position
//---------------------------------------------------------------------
inline void CStringIO::truncate()
{
	resize(_position);
}

//---------------------------------------------------------------------
// check if position >= size
//---------------------------------------------------------------------
inline bool CStringIO::eof() const
{
	return (_position >= size());
}

//---------------------------------------------------------------------
// write some bytes to current position
//---------------------------------------------------------------------
inline void CStringIO::write(const void *ptr, size_t length)
{
	size_t endup = _position + length;
	if (endup > size()) {
		resize(endup);
	}
	if (ptr) {
		memcpy(data() + _position, ptr, length);
	}
	_position += length;
}

//---------------------------------------------------------------------
// read from current position, returns how many bytes acturelly read
//---------------------------------------------------------------------
inline size_t CStringIO::read(void *ptr, size_t length)
{
	size_t canread;
	if (_position >= size()) return 0;
	canread = size() - _position;
	if (length > canread) length = canread;
	if (ptr) {
		memcpy(ptr, data() + _position, length);
	}
	_position += length;
	return length;
}

//---------------------------------------------------------------------
// returns how many bytes can be read
//---------------------------------------------------------------------
inline size_t CStringIO::canread()
{
	if (_position >= size()) return 0;
	return size() - _position;
}

//---------------------------------------------------------------------
// get const data reference
//---------------------------------------------------------------------
inline const char & CStringIO::operator [] (size_t pos) const
{
	return _data[pos];
}

//---------------------------------------------------------------------
// get data reference
//---------------------------------------------------------------------
inline char & CStringIO::operator[] (size_t pos)
{
	return _data[pos];
}


//---------------------------------------------------------------------
// write unsigned char
//---------------------------------------------------------------------
inline void CStringIO::write_u8(unsigned char value)
{
	write(&value, 1);
}

//---------------------------------------------------------------------
// write char
//---------------------------------------------------------------------
inline void CStringIO::write_i8(char value)
{
	write(&value, 1);
}

//---------------------------------------------------------------------
// write unsigned short
//---------------------------------------------------------------------
inline void CStringIO::write_u16(unsigned short value)
{
	unsigned char b[2];
	if (_little_endian) {
		b[0] = (unsigned char)(value & 0xff);
		b[1] = (unsigned char)((value >> 8) & 0xff);
	}	else {
		b[1] = (unsigned char)(value & 0xff);
		b[0] = (unsigned char)((value >> 8) & 0xff);
	}
	write(b, 2);
}

//---------------------------------------------------------------------
// write short
//---------------------------------------------------------------------
inline void CStringIO::write_i16(short value)
{
	write_u16((unsigned short)value);
}

//---------------------------------------------------------------------
// write unsigned int32
//---------------------------------------------------------------------
inline void CStringIO::write_u32(unsigned long value)
{
	unsigned char b[4];
	if (_little_endian) {
		b[0] = (unsigned char)((value >>  0) & 0xff);
		b[1] = (unsigned char)((value >>  8) & 0xff);
		b[2] = (unsigned char)((value >> 16) & 0xff);
		b[3] = (unsigned char)((value >> 24) & 0xff);
	}	else {
		b[3] = (unsigned char)((value >>  0) & 0xff);
		b[2] = (unsigned char)((value >>  8) & 0xff);
		b[1] = (unsigned char)((value >> 16) & 0xff);
		b[0] = (unsigned char)((value >> 24) & 0xff);
	}
	write(b, 4);
}

//---------------------------------------------------------------------
// write int32
//---------------------------------------------------------------------
inline void CStringIO::write_i32(long value)
{
	write_u32((unsigned long)value);
}

//---------------------------------------------------------------------
// read unsigned char
//---------------------------------------------------------------------
inline unsigned char CStringIO::read_u8()
{
	unsigned char value = 0;
	read(&value, 1);
	return value;
}

//---------------------------------------------------------------------
// read char
//---------------------------------------------------------------------
inline char CStringIO::read_i8()
{
	return (char)read_u8();
}

//---------------------------------------------------------------------
// read unsigned short
//---------------------------------------------------------------------
inline unsigned short CStringIO::read_u16()
{
	unsigned char b[2] = { 0, 0 };
	read(b, 2);
	unsigned short value;
	if (_little_endian) {
		value = b[0] + (((unsigned short)b[1]) << 8);
	}	else {
		value = b[1] + (((unsigned short)b[0]) << 8);
	}
	return value;
}

//---------------------------------------------------------------------
// read short
//---------------------------------------------------------------------
inline short CStringIO::read_i16()
{
	return (short)read_u16();
}

//---------------------------------------------------------------------
// read unsigned int32
//---------------------------------------------------------------------
inline unsigned long CStringIO::read_u32()
{
	unsigned char b[4] = { 0, 0, 0, 0 };
	read(b, 4);
	unsigned long value;
	if (_little_endian) {
		value = (((unsigned long)b[0]) <<  0) | 
				(((unsigned long)b[1]) <<  8) |
				(((unsigned long)b[2]) << 16) |
				(((unsigned long)b[3]) << 24);
	}	else {
		value = (((unsigned long)b[3]) <<  0) | 
				(((unsigned long)b[2]) <<  8) |
				(((unsigned long)b[1]) << 16) |
				(((unsigned long)b[0]) << 24);
	}
	return value;
}

//---------------------------------------------------------------------
// read int32
//---------------------------------------------------------------------
inline long CStringIO::read_i32()
{
	return (long)read_u32();
}

//---------------------------------------------------------------------
// read int32
//---------------------------------------------------------------------
#define CSTRINGIO_MASK(__bits) ((((IUINT64)1) << (__bits)) - 1)

inline char* CStringIO::encode_u64(char *ptr, IUINT64 v)
{
	unsigned char *p = (unsigned char*)ptr;
	p[0] = (unsigned char)(v & 0x7f);
	if (v <= CSTRINGIO_MASK(7))
		return ptr + 1;
	p[0] |= 0x80;
	p[1] = (unsigned char)((v >> 7) & 0x7f);
	if (v <= CSTRINGIO_MASK(14))
		return ptr + 2;
	p[1] |= 0x80;
	p[2] = (unsigned char)((v >> 14) & 0x7f);
	if (v <= CSTRINGIO_MASK(21)) 
		return ptr + 3;
	p[2] |= 0x80;
	p[3] = (unsigned char)((v >> 21) & 0x7f);
	if (v <= CSTRINGIO_MASK(28)) 
		return ptr + 4;
	p[3] |= 0x80;
	p[4] = (unsigned char)((v >> 28) & 0x7f);
	if (v <= CSTRINGIO_MASK(35))
		return ptr + 5;
	p[4] |= 0x80;
	p[5] = (unsigned char)((v >> 35) & 0x7f);
	if (v <= CSTRINGIO_MASK(42))
		return ptr + 6;
	p[5] |= 0x80;
	p[6] = (unsigned char)((v >> 42) & 0x7f);
	if (v <= CSTRINGIO_MASK(49))
		return ptr + 7;
	p[6] |= 0x80;
	p[7] = (unsigned char)((v >> 49) & 0x7f);
	if (v <= CSTRINGIO_MASK(56))
		return ptr + 8;
	p[7] |= 0x80;
	p[8] = (unsigned char)((v >> 56) & 0x7f);
	if (v <= CSTRINGIO_MASK(63))
		return ptr + 9;
	p[8] |= 0x80;
	p[9] = (unsigned char)((v >> 63) & 0x7f);
	return ptr + 10;
}

inline const char* CStringIO::decode_u64(const char *ptr, IUINT64 *v)
{
	const unsigned char *p = (const unsigned char*)ptr;
	IUINT64 x = 0;

	x |= ((unsigned int)(p[0] & 0x7f));
	if ((p[0] & 0x80) == 0) {
		v[0] = x;
		return ptr + 1;
	}

	x |= ((unsigned int)(p[1] & 0x7f)) << 7;
	if ((p[1] & 0x80) == 0) {
		v[0] = x;
		return ptr + 2;
	}

	x |= ((unsigned int)(p[2] & 0x7f)) << 14;
	if ((p[2] & 0x80) == 0) {
		v[0] = x;
		return ptr + 3;
	}

	x |= ((unsigned int)(p[3] & 0x7f)) << 21;
	if ((p[3] & 0x80) == 0) {
		v[0] = x;
		return ptr + 4;
	}

	x |= ((IUINT64)(p[4] & 0x7f)) << 28;
	if ((p[4] & 0x80) == 0) {
		v[0] = x;
		return ptr + 5;
	}

	x |= ((IUINT64)(p[5] & 0x7f)) << 35;
	if ((p[5] & 0x80) == 0) {
		v[0] = x;
		return ptr + 6;
	}

	x |= ((IUINT64)(p[6] & 0x7f)) << 42;
	if ((p[6] & 0x80) == 0) {
		v[0] = x;
		return ptr + 7;
	}

	x |= ((IUINT64)(p[7] & 0x7f)) << 49;
	if ((p[7] & 0x80) == 0) {
		v[0] = x;
		return ptr + 8;
	}

	x |= ((IUINT64)(p[8] & 0x7f)) << 56;
	if ((p[8] & 0x80) == 0) {
		v[0] = x;
		return ptr + 9;
	}

	x |= ((IUINT64)(p[9] & 0x7f)) << 63;
	v[0] = x;

	return ptr + 10;
}

inline void CStringIO::write_u64(IUINT64 value)
{
	char buffer[16];
	char *ptr = buffer;
	ptr = encode_u64(ptr, value);
	write(buffer, ptr - buffer);
}

inline void CStringIO::write_i64(IINT64 value)
{
	write_u64((IUINT64)value);
}

inline IUINT64 CStringIO::read_u64()
{
	const char *buffer = &_data[_position];
	const char *ptr = buffer;
	IUINT64 value;
	ptr = decode_u64(ptr, &value);
	long size = (long)(ptr - buffer);
	_position += size;
	if (_position > _size) {
		throw std::bad_exception();
		_position = size;
	}
	return value;
}

inline IINT64 CStringIO::read_i64()
{
	return (IINT64)read_u64();
}


inline CStringIO& CStringIO::operator << (unsigned char value)
{
	write_u8(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (unsigned short value)
{
	write_u16(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (unsigned long value)
{
	write_u32(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (unsigned int value)
{
	write_u32((unsigned long)value);
	return *this;
}

inline CStringIO& CStringIO::operator << (char value)
{
	write_i8(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (short value)
{
	write_i16(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (long value)
{
	write_i32(value);
	return *this;
}

inline CStringIO& CStringIO::operator << (int value)
{
	write_i32((long)value);
	return *this;
}

inline CStringIO& CStringIO::operator >> (unsigned char &value)
{
	value = read_u8();
	return *this;
}

inline CStringIO& CStringIO::operator >> (unsigned short &value)
{
	value = read_u16();
	return *this;
}

inline CStringIO& CStringIO::operator >> (unsigned long &value)
{
	value = read_u32();
	return *this;
}

inline CStringIO& CStringIO::operator >> (unsigned int &value)
{
	value = (unsigned int)read_u32();
	return *this;
}

inline CStringIO& CStringIO::operator >> (char &value)
{
	value = read_i8();
	return *this;
}

inline CStringIO& CStringIO::operator >> (short &value)
{
	value = read_i16();
	return *this;
}

inline CStringIO& CStringIO::operator >> (long &value)
{
	value = read_i32();
	return *this;
}

inline CStringIO& CStringIO::operator >> (int &value)
{
	value = (int)read_i32();
	return *this;
}

#endif


