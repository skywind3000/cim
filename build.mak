mode: dll
int: build/objs/$(target)

out: bin/libcim.$(target).so
win32/out: bin/libcim.$(target).dll
win64/out: bin/libcim.$(target).dll

lib: modules/lib

src: cim.cpp
src: internal.cpp
src: DecodeBPG.cpp
src: DecodeFLIF.cpp
src: DecodePNG.cpp

android/flag: -std=c++11
android/flag: -g

win32/flnk: -s, -static
win32/wlnk: -Bstatic -lpthread
win64/flnk: -s, -static
win64/wlnk: -Bstatic -lpthread


link: stdc++
android/link: m
# android/flnk: -s

link: bpg.$(target), png.$(target), flif.$(target), z.$(target) 



