#ifndef _CIM_LOADER_H_
#define _CIM_LOADER_H_

#include <stdio.h>
#include <stdlib.h>

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <windows.h>
#else 
#include <unistd.h>
#endif

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#endif


//---------------------------------------------------------------------
// interfaces
//---------------------------------------------------------------------
static void *cim_dll_instance = NULL;

typedef int (*cim_image_info_t)(const void*, int*, int*, int*);
typedef int (*cim_image_decode_t)(const void*, int, void*, int);

static cim_image_info_t cim_image_info_p = NULL;
static cim_image_decode_t cim_image_decode_p = NULL;


//---------------------------------------------------------------------
// initialize and load dll
//---------------------------------------------------------------------
static int cim_init_dll(const char *dllname)
{
	void *hdll = NULL;
	if (cim_dll_instance) return 0;
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
	hdll = LoadLibraryA(dllname);
#else
	hdll = dlopen(dllname, RTLD_LAZY);
#endif
	if (hdll == NULL) return -1;
	else {
		cim_image_info_t info_p = NULL;
		cim_image_decode_t decode_p = NULL;
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
		info_p = (cim_image_info_t)GetProcAddress((HINSTANCE)hdll, "cim_image_info");
		decode_p = (cim_image_decode_t)GetProcAddress((HINSTANCE)hdll, "cim_image_decode");
#else
		info_p = (cim_image_info_t)dlsym(hdll, "cim_image_info");
		decode_p = (cim_image_decode_t)dlsym(hdll, "cim_image_decode");
#endif
		if (info_p == NULL) return -2;
		if (decode_p == NULL) return -3;
		cim_image_info_p = info_p;
		cim_image_decode_p = decode_p;
		cim_dll_instance = hdll;
	}
	return 0;
}


//---------------------------------------------------------------------
// info
//---------------------------------------------------------------------
static int cim_image_info(const void *data, int *w, int *h, int *bpp)
{
	if (cim_image_info_p == NULL) return 1;
	return cim_image_info_p(data, w, h, bpp);
}


//---------------------------------------------------------------------
// decode
//---------------------------------------------------------------------
static int cim_image_decode(const void *data, int nbytes, void *pixel, int stride)
{
	if (cim_image_decode_p == NULL) return 1;
	return cim_image_decode_p(data, nbytes, pixel, stride);
}


#endif


