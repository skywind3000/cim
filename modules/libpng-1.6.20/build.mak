mode: lib
int: build/objs/$(target)
out: ../lib/libpng.$(target).a

flag: -O2
inc: ../zlib-1.2.8

android/flag: -mfloat-abi=softfp


src: png.c
src: pngerror.c
src: pngget.c
src: pngmem.c
src: pngpread.c
src: pngread.c
src: pngrio.c
src: pngrtran.c
src: pngrutil.c
src: pngset.c
src: pngtrans.c
src: pngwio.c
src: pngwrite.c
src: pngwtran.c
src: pngwutil.c


