mode: lib
int: build/objs/$(target)
out: ../lib/libbpg.$(target).a

flag: -Os
inc: ../include

inc: jctvc
inc: .

cflag: -std=c99
cflag: -fno-asynchronous-unwind-tables, -fdata-sections
cflag: -ffunction-sections, -fno-math-errno, -fno-signed-zeros, -fno-tree-vectorize
cflag: -fomit-frame-pointer

define: _ISOC99_SOURCE
define: _POSIX_C_SOURCE=200112
define: _XOPEN_SOURCE=600
define: HAVE_AV_CONFIG_H
define: _GNU_SOURCE=1
define: USE_VAR_BIT_DEPTH
define: USE_PRED
define: _FILE_OFFSET_BITS=64
define: _LARGEFILE_SOURCE
define: _REENTRANT
# define: USE_JCTVC

# android/flag: -fpermissive
android/flag: -g

# src: jctvc/encmain.cpp

src: libavcodec/hevc_cabac.c
src: libavcodec/hevc_filter.c
src: libavcodec/hevc.c
src: libavcodec/hevcpred.c
src: libavcodec/hevc_refs.c
src: libavcodec/hevcdsp.c
src: libavcodec/hevc_mvs.c
src: libavcodec/hevc_ps.c
src: libavcodec/hevc_sei.c
src: libavcodec/utils.c
src: libavcodec/cabac.c
src: libavcodec/golomb.c
src: libavcodec/videodsp.c

src: libavutil/mem.c
src: libavutil/buffer.c
src: libavutil/log2_tab.c
src: libavutil/frame.c
src: libavutil/pixdesc.c
src: libavutil/md5.c

# src: bpgdec.c
# src: bpgenc.c
src: libbpg.c
# src: tmalloc.c
# src: jctvc_glue.cpp

