mode: lib
int: build/objs/$(target)
out: ../lib/libbpg.$(target).a

flag: -O2
inc: ../include

inc: jctvc
inc: .

define: _ISOC99_SOURCE
define: _POSIX_C_SOURCE=200112
define: _XOPEN_SOURCE=600
define: HAVE_AV_CONFIG_H
define: _GNU_SOURCE=1
define: USE_VAR_BIT_DEPTH
define: USE_PRED
define: USE_JCTVC

android/flag: -std=c++11
android/flag: -fpermissive

src: jctvc/libmd5/libmd5.c
src: jctvc/TLibCommon/ContextModel.cpp
src: jctvc/TLibCommon/ContextModel3DBuffer.cpp
src: jctvc/TLibCommon/Debug.cpp
src: jctvc/TLibCommon/SEI.cpp
src: jctvc/TLibCommon/TComBitStream.cpp
src: jctvc/TLibCommon/TComCABACTables.cpp
src: jctvc/TLibCommon/TComChromaFormat.cpp
src: jctvc/TLibCommon/TComDataCU.cpp
src: jctvc/TLibCommon/TComInterpolationFilter.cpp
src: jctvc/TLibCommon/TComLoopFilter.cpp
src: jctvc/TLibCommon/TComMotionInfo.cpp
src: jctvc/TLibCommon/TComPattern.cpp
src: jctvc/TLibCommon/TComPic.cpp
src: jctvc/TLibCommon/TComPicSym.cpp
src: jctvc/TLibCommon/TComPicYuv.cpp
src: jctvc/TLibCommon/TComPicYuvMD5.cpp
src: jctvc/TLibCommon/TComPrediction.cpp
src: jctvc/TLibCommon/TComRdCost.cpp
src: jctvc/TLibCommon/TComRdCostWeightPrediction.cpp
src: jctvc/TLibCommon/TComRom.cpp
src: jctvc/TLibCommon/TComSampleAdaptiveOffset.cpp
src: jctvc/TLibCommon/TComSlice.cpp
src: jctvc/TLibCommon/TComTU.cpp
src: jctvc/TLibCommon/TComTrQuant.cpp
src: jctvc/TLibCommon/TComWeightPrediction.cpp
src: jctvc/TLibCommon/TComYuv.cpp
src: jctvc/TLibEncoder/NALwrite.cpp
src: jctvc/TLibEncoder/SEIwrite.cpp
src: jctvc/TLibEncoder/SyntaxElementWriter.cpp
src: jctvc/TLibEncoder/TEncAnalyze.cpp
src: jctvc/TLibEncoder/TEncBinCoderCABAC.cpp
src: jctvc/TLibEncoder/TEncBinCoderCABACCounter.cpp
src: jctvc/TLibEncoder/TEncCavlc.cpp
src: jctvc/TLibEncoder/TEncCu.cpp
src: jctvc/TLibEncoder/TEncEntropy.cpp
src: jctvc/TLibEncoder/TEncGOP.cpp
src: jctvc/TLibEncoder/TEncPic.cpp
src: jctvc/TLibEncoder/TEncPreanalyzer.cpp
src: jctvc/TLibEncoder/TEncRateCtrl.cpp
src: jctvc/TLibEncoder/TEncSampleAdaptiveOffset.cpp
src: jctvc/TLibEncoder/TEncSbac.cpp
src: jctvc/TLibEncoder/TEncSearch.cpp
src: jctvc/TLibEncoder/TEncSlice.cpp
src: jctvc/TLibEncoder/TEncTop.cpp
src: jctvc/TLibEncoder/WeightPredAnalysis.cpp
src: jctvc/TLibVideoIO/TVideoIOYuv.cpp
src: jctvc/TAppEncCfg.cpp
src: jctvc/TAppEncTop.cpp
src: jctvc/program_options_lite.cpp
# src: jctvc/encmain.cpp

src: libavcodec/hevc_cabac.c
src: libavcodec/hevc_filter.c
src: libavcodec/hevc.c
src: libavcodec/hevcpred.c
src: libavcodec/hevc_refs.c
src: libavcodec/hevcdsp.c
src: libavcodec/hevc_mvs.c
src: libavcodec/hevc_ps.c
src: libavcodec/hevc_sei.c
src: libavcodec/utils.c
src: libavcodec/cabac.c
src: libavcodec/golomb.c
src: libavcodec/videodsp.c

src: libavutil/mem.c
src: libavutil/buffer.c
src: libavutil/log2_tab.c
src: libavutil/frame.c
src: libavutil/pixdesc.c
src: libavutil/md5.c

# src: bpgdec.c
# src: bpgenc.c
src: libbpg.c
# src: tmalloc.c
src: jctvc_glue.cpp

