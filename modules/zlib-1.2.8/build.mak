mode: lib
int: build/objs/$(target)
out: ../lib/libz.$(target).a

flag: -O2

android/flag: -mfloat-abi=softfp
android/flag: -g

src: adler32.c
src: compress.c
src: crc32.c
src: deflate.c
src: gzclose.c
src: gzlib.c
src: gzread.c
src: gzwrite.c
src: infback.c
src: inffast.c
src: inflate.c
src: inftrees.c
src: trees.c
src: uncompr.c
src: zutil.c


