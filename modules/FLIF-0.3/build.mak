mode: lib
int: build/objs/$(target)
out: ../lib/libflif.$(target).a

flag: -O2
inc: ../include

define: INT16_MAX=32767, INT16_MIN=(-32768)
# define: FLIF_BUILD_DLL=1
define: HAS_ENCODER

win32/flag: -msse2
android/flag: -mfloat-abi=softfp, -std=c++11

src: src/library/flif-interface_enc.cpp
src: src/library/flif-interface_dec.cpp
src: src/library/flif-interface_common.cpp
src: src/library/flif-interface.cpp

src: src/image/color_range.cpp
src: src/image/crc32k.cpp
src: src/image/image-metadata.cpp
src: src/image/image-pam.cpp
src: src/image/image-png.cpp
src: src/image/image-pnm.cpp
src: src/image/image-rggb.cpp
src: src/image/image.cpp

src: src/maniac/bit.cpp
src: src/maniac/chance.cpp
src: src/maniac/symbol.cpp

src: src/transform/factory.cpp

src: src/common.cpp
src: src/flif-dec.cpp
src: src/flif-enc.cpp
# src: src/flif.cpp
src: src/io.cpp

src: extern/lodepng.cpp


