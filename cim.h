//=====================================================================
//
// cim.h - Compact Image Module
//
// Created by skywind on 2019/01/05
// Last Modified: 2019/01/05 16:33:51
//
//=====================================================================
#ifndef _CIM_H_
#define _CIM_H_

#include <stdio.h>

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
#ifdef CIM_EXPORT	
#define CIM_ENTRY	__declspec(dllexport)
#else
#define CIM_ENTRY	__declspec(dllimport)
#endif
#else
#define CIM_ENTRY
#endif


#ifdef __cplusplus
extern "C" {
#endif


// 读取图片信息：成功返回 0，失败返回非零
CIM_ENTRY int cim_image_info(
		const void *data,    // 图片数据
		int *w,              // 返回长度
		int *h,              // 返回宽度
		int *bpp             // 每像素多少位，24 还是 32
		);

// 解码图片：成功返回 0，失败返回非零
CIM_ENTRY int cim_image_decode(
		const void *data,    // 图片数据指针
		int nbytes,          // 数据有多少字节
		void *pixel,         // 接收图片的缓存
		int stride           // 缓存中每一行占用多少字节
		);

// 编码图片
CIM_ENTRY int cim_image_encode(
		const char *filename,    // 保存的文件名
		const void *pixel,       // 像素指针
		int format,              // 保存格式：0/PNG, 1/FLIF, 2/BPG
		int bpp,                 // 像素位宽：24/32
		int width,               // 图片宽度
		int height,              // 图片高度
		int stride,              // 图片缓存每一行占用多少字节
		int quality,             // 图片质量：0-100
		int profile              // 编码运算复杂度：0-100
		);


#ifdef __cplusplus
}
#endif

#endif


