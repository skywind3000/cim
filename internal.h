#pragma once

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <vector>

#include "BasicBitmap.h"

#define PIC_PNG		0
#define PIC_BPG		1
#define PIC_FLIF	2
#define PIC_JPEG	3
#define PIC_BMP		4
#define PIC_UNKNOW  9

int cim_stb_info(const void *data, int *w, int *h, int *bpp);
int cim_stb_decode(const void *data, int nbytes, void *pixel, int stride);

int cim_flif_info(const void *data, int *w, int *h, int *bpp);
int cim_flif_decode(const void *data, int nbytes, void *pixel, int stride);

int cim_bpg_info(const void *data, int *w, int *h, int *bpp);
int cim_bpg_decode(const void *data, int nbytes, void *pixel, int stride);

bool cim_load_content(const char *filename, std::string &content);

int cim_image_detect(const void *data);



