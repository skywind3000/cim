#include <stdio.h>
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#include "../modules/include/stb_image.h"
#include "../BasicBitmap.cpp"

void test1()
{
	int x, y, n;
	unsigned char *ptr;
	ptr = stbi_load("../samples/battery.png", &x, &y, &n, 0);
	printf("%dx%d channel=%d\n", x, y, n);
	stbi_image_free(ptr);
}

void test2()
{
	int x, y, n;
	unsigned char *ptr;
	ptr = stbi_load("../samples/width-95.jpg", &x, &y, &n, 0);
	if (!ptr) return;
	BasicBitmap *bmp = new BasicBitmap(x, y, BasicBitmap::R8G8B8);
	for (int j = 0; j < y; j++) {
		for (int i = 0; i < x; i++, ptr += 3) {
			int r = ptr[0];
			int g = ptr[1];
			int b = ptr[2];
			int c = (r << 16) | (g << 8) | b;
			bmp->SetPixel(i, j, c);
		}
	}
	bmp->SaveBmp("../samples/output.bmp");
	stbi_image_free(ptr);
}

int main(void)
{
	test2();
	return 0;
}

