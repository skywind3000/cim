#include <windows.h>
#include "../internal.h"


void test1()
{
	std::string content;
	int w, h, bpp;
	cim_load_content("../samples/battery.bpg", content);
	int hr = cim_bpg_info(&content[0], &w, &h, &bpp);
	if (hr != 0) {
		printf("failed\n");
		return ;
	}
	printf("%dx%d bpp=%d\n", w, h, bpp);
}

void test2()
{
	std::string content;
	cim_load_content("../samples/bg.bpg", content);
	if (content.size() == 0) return ;
	int w, h, bpp;
	int hr = cim_bpg_info(&content[0], &w, &h, &bpp);
	if (hr != 0) {
		printf("failed\n");
		return ;
	}

	BasicBitmap bmp(w, h, (bpp == 32)? BasicBitmap::A8R8G8B8 : BasicBitmap::R8G8B8);

	DWORD ts = timeGetTime();
	hr = cim_bpg_decode(content.c_str(), (int)content.size(), bmp.Address8(0, 0), bmp.Pitch());
	ts = timeGetTime() - ts;

	printf("%dx%d bpp=%d\n", w, h, bpp);
	printf("time: %d hr=%d\n", (int)ts, hr);

	if (bmp.Bpp() == 32) {
		bmp.Shuffle(2, 1, 0, 3);
	}	else {
		bmp.Shuffle(2, 1, 0, 3);
	}

	bmp.SaveBmp("output.bmp");
}

//! lib: ../modules/lib
//! link: bpg.win32, z.win32, png.win32
//! exe: ../internal.cpp, ../DecodeBPG.cpp, ../BasicBitmap.cpp
int main(void)
{
	test2();

	return 0;
}





