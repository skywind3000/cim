#include <stdio.h>
#include <string>

#include "../CimLoader.h"
#include "../BasicBitmap.h"


bool load_content(const char *filename, std::string &content)
{
	size_t size;
	FILE *fp = fopen(filename, "rb");
	if (fp == NULL) return false;
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	content.resize(size);
	if (size > 0) {
		char *ptr = &content[0];
		while (size > 0) {
			size_t hr = fread(ptr, 1, size, fp);
			if (hr == 0) {
				fclose(fp);
				return false;
			}
			size -= hr;
			ptr += hr;
		}
	}
	fclose(fp);
	return true;
}

void test1(void)
{
	std::string content;
	load_content("../samples/bg.bpg", content);
	int w, h, bpp;
	int hr = cim_image_info(content.c_str(), &w, &h, &bpp);
	if (hr != 0) {
		printf("get info faied\n");
		return;
	}
	printf("%dx%dx%d\n", w, h, bpp);
	BasicBitmap image(w, h, (bpp == 32)? BasicBitmap::A8R8G8B8 : BasicBitmap::R8G8B8);
	hr = cim_image_decode(content.c_str(), (int)content.size(), image.Address8(0, 0), image.Pitch());
	image.Shuffle(2, 1, 0, 3);
	image.SaveBmp("output.bmp");
}

//! flag: -static
//! flnk: -static
//! exe: ../BasicBitmap.cpp
int main(void)
{
	int hr = cim_init_dll("e:/lab/svn/cim/bin/libcim.win32.dll");
	printf("init: %d\n", hr);
	if (hr != 0) {
		printf("LastError: %d\n", GetLastError());
		return -1;	
	}
	test1();
	return 0;
}




