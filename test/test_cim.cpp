#include <stdio.h>
#include <windows.h>
#include <string>

#define CIM_EXPORT
#include "../cim.h"
#include "../internal.h"

const char *names[] = {
	"../samples/bg.png",
	"../samples/bg.bpg",
	"../samples/bg.flif",
	"../samples/battery.png",
	"../samples/battery.bpg",
	"../samples/battery.flif",
	"../samples/keyboard.png",
	"../samples/keyboard.bpg",
	"../samples/keyboard.flif",
	"../samples/keyboard.jpg",
	NULL,
};

void test1()
{
	for (int i = 0; names[i]; i++) {
		std::string content;
		DWORD t1 = timeGetTime();
		cim_load_content(names[i], content);
		t1 = timeGetTime() - t1;
		DWORD t2 = timeGetTime();
		int w, h, bpp;
		int hr = cim_image_info(content.c_str(), &w, &h, &bpp);
		if (hr != 0) {
			printf("get info error: %s\n", names[i]);
			continue;
		}
		BasicBitmap image(w, h, (bpp == 32)? BasicBitmap::A8R8G8B8 : BasicBitmap::R8G8B8);
		hr = cim_image_decode(content.c_str(), (int)content.size(), image.Address8(0, 0), image.Pitch());
		if (hr != 0) {
			printf("decode error: %s\n", names[i]);
			continue;
		}
		t2 = timeGetTime() - t2;
		printf("%s: %dx%dx%d size=%d load=%dms decode=%dms total=%dms\n", names[i], 
				w, h, bpp, (int)content.size(), (int)t1, (int)t2, (int)t1 + (int)t2);
		char output[80];
		sprintf(output, "out-%d.bmp", i);
		image.SaveBmp(output);
	}
}


//! lib: ../modules/lib
//! link: bpg.win32, z.win32, png.win32, flif.win32
//! src: ../DecodeFLIF.cpp, ../cim.cpp
//! exe: ../internal.cpp, ../DecodeBPG.cpp, ../BasicBitmap.cpp
int main(void)
{
	test1();
	return 0;
}


