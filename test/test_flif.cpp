#include <stdio.h>
#include <windows.h>
#include <mmsystem.h>
#include "../internal.h"

void test1()
{
	std::string content;
	if (!cim_load_content("../samples/bg.flif", content))
		return ;

	printf("size: %d\n", (int)content.size());

	int w, h, bpp;
	int hr = cim_flif_info(content.c_str(), &w, &h, &bpp);

	if (hr != 0) {
		printf("error flif get info: %d\n", hr);
		return;
	}

	printf("info: %dx%d bpp=%d\n", w, h, bpp);

	BasicBitmap bmp(w, h, (bpp == 32)? BasicBitmap::A8R8G8B8 : BasicBitmap::R8G8B8);

	std::vector<char> buffer;
	int stride = w * bpp / 8 + 8;
	buffer.resize(stride * h + 8);

	DWORD ts = timeGetTime();
	cim_flif_decode(content.c_str(), (int)content.size(), bmp.Address8(0, 0), bmp.Pitch());
	ts = timeGetTime() - ts;
	printf("time: %d\n", ts);

	// cim_flif_decode(content.c_str(), &buffer[0], stride);
	if (bmp.Bpp() == 32) {
		bmp.Shuffle(2, 1, 0, 3);	
	}	else {
		bmp.Shuffle(2, 1, 0, 0);
	}
	bmp.SaveBmp("output.bmp");
}

//! flag: -g
//! exe: ../BasicBitmap.cpp, ../internal.cpp, ../DecodeFLIF.cpp
//! flag: -O2
//! lib: ../modules/lib
//! link: flif.win32, png.win32, z.win32
int main(void)
{
	test1();
	return 0;
}


