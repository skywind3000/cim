#define CIM_EXPORT
#include "cim.h"
#include "internal.h"


CIM_ENTRY int cim_image_info(const void *data, int *w, int *h, int *bpp)
{
	int pictype = cim_image_detect(data);
	if (pictype == PIC_BPG)
		return cim_bpg_info(data, w, h, bpp);
	else if (pictype == PIC_FLIF)
		return cim_flif_info(data, w, h, bpp);
	return cim_stb_info(data, w, h, bpp);
}

CIM_ENTRY int cim_image_decode(const void *data, int nbytes, void *pixel, int stride)
{
	int pictype = cim_image_detect(data);
	if (pictype == PIC_BPG)
		return cim_bpg_decode(data, nbytes, pixel, stride);
	else if (pictype == PIC_FLIF)
		return cim_flif_decode(data, nbytes, pixel, stride);
	return cim_stb_decode(data, nbytes, pixel, stride);
}


