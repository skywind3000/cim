#include "internal.h"

extern "C" {
#include "modules/include/libbpg.h"
}


int cim_bpg_info(const void *data, int *w, int *h, int *bpp)
{
	BPGImageInfo p;
	BPGExtensionData *t;
	const uint8_t *buf = (const uint8_t*)data;
	int size = 1024 * 1024 * 20;
	int hr = bpg_decoder_get_info_from_buf(&p, &t, buf, size);
	if (hr < 0) return hr;
	*w = p.width;
	*h = p.height;
	*bpp = p.has_alpha? 32 : 24;
	return 0;
}


int cim_bpg_decode(const void *data, int nbytes, void *pixel, int stride)
{
	BPGDecoderContext *s;
	int size = nbytes;

	s = bpg_decoder_open();
	if (s == NULL) return -1;

	int hr = bpg_decoder_decode(s, (const uint8_t*)data, size);
	if (hr < 0) {
		return -2;
	}

	BPGImageInfo info;
	if (bpg_decoder_get_info(s, &info) < 0) return -3;

	int bpp = info.has_alpha? 32 : 24;

	hr = bpg_decoder_start(s, (bpp == 32) ? 
			BPG_OUTPUT_FORMAT_RGBA32 :
			BPG_OUTPUT_FORMAT_RGB24);

	if (hr < 0) return -3;

	int required = (bpp == 32)? 4 * info.width : 3 * info.width;
	int pitch = (stride < 0)? (-stride) : stride;

	if (pitch < required) return -4;
	std::vector<char> buffer(required + 8);
	buffer.resize(required + 8);

	for (int j = 0; j < (int)info.height; j++) {
#if 0
		bpg_decoder_get_line(s, pixel);
#else
		char *buf = &buffer[0];
		bpg_decoder_get_line(s, buf);
		memcpy(pixel, buf, required);
#endif
		pixel = (char*)pixel + stride;
	}

	bpg_decoder_close(s);

	return 0;
}


